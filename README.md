# tp-make-mcd

#### 介绍
自定义创建命令

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

to应用
php thinkphp maketo [obj] [to] [sys]
示例 php thinkphp maketo user toadmin
创建 controller route validate
'maketo'=>'app\common\command\ToObj',
sys模块
php thinkphp makesys 对象模块名 [sys名]
创建 model dao service
'makesys'=>'app\common\command\SysObj',
'toobj'=>'app\common\command\NewObj',

sys模块to应用
php think tobj [obj] [sys] [to]

任务jbo
php think makejob [obj] [sys]

订阅
php think makescb [obj] [sys]

步1
 * 动态注册
 *  Event::subscribe('app\sysuser\subscribe\User');
 * event文件配置
 *  subxcribe => ['app\sysuser\subscribe\User']
步2
	 event('UserLogin', $user);
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
