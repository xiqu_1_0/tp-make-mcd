<?php
declare (strict_types = 1);

namespace betka\makecmd;

use think\Service;

class CmdService extends Service
{

    public function register()
    {
        $commands = [
            'makesys'=>'betka\makecmd\SysObj',
            'maketo'=>'betka\makecmd\ToObj',
            'toobj'=>'betka\makecmd\NewObj',
            'makejob'=>'betka\makecmd\NewJob',
            'makescb'=>'betka\makecmd\NewScb',
        ];
        $this->commands($commands);
    }

    public function boot()
    {

    }
}
