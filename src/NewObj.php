<?php
declare (strict_types = 1);

namespace betka\makecmd;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\facade\Console;

class newObj extends Command
{
    /**
     * 应用基础目录
     * @var string
     */
    protected $basePath;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('toobj')
            ->addArgument('name', Argument::REQUIRED, "The name of the class")
            ->addArgument('sysobj', Argument::OPTIONAL, "The name of the mdoule")
            ->addArgument('toapp', Argument::OPTIONAL, "The name of the mdoule")
            ->setDescription('创建应用或模块对象');
    }

    protected function execute(Input $input, Output $output)
    {
        $name = trim($input->getArgument('name'));
        $toapp = trim($input->getArgument('toapp')??'');
        $sysobj = trim($input->getArgument('sysobj')??'');
        $output->writeln("<info>  start obj arg... [obj]{$name} [sys]{$sysobj} [to]{$toapp} </info>");
        $output = Console::call('makesys', [$name,$sysobj]);
        echo $output->fetch();
        if($toapp){
            $output = Console::call('maketo', [$name,$toapp,$sysobj]);
            echo $output->fetch();
        }
    }

}