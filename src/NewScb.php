<?php
declare (strict_types = 1);

namespace betka\makecmd;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\console\input\Option;

//生成订阅文件
class NewScb extends Command
{
    /**
     * 应用基础目录
     * @var string
     */
    protected $basePath;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('makescb')
            ->addArgument('name', Argument::REQUIRED, "The name of the class")
             ->addArgument('tosys', Argument::REQUIRED, "to app")
            ->setDescription('模块下对象创建');
    }

    protected function execute(Input $input, Output $output)
    {
        $this->basePath = $this->app->getBasePath();
        $name = trim($input->getArgument('name'));
        $toapp = trim($input->getArgument('tosys'));
        //$toapp = $input->getOption('tosys')??'systmp';
        // $to = substr($toapp, 0, 3);
        // if ($to != 'sys') {
        //     $output->writeln("<info>set moudle sys prefix! " . $toapp . '=>' . $to . "</info>");
        //     return;
        // }
        if (!$name) {
            $output->writeln("<info>empty!</info>");
            return;
        }

        $preNamespace = 'app' . '\\' . $toapp; //app\sysuser
        $className = $name;
        $className = $this->getClassName($className); //目录只支持一层
        $this->buildJob($toapp, $preNamespace, $className);

        $output->writeln("<info>Successed</info>");
    }

    protected function getClassName(string $name): string
    {
        if (strpos($name, '\\') !== false) {
            return $name;
        }
        if (strpos($name, '/') !== false) {
            $name = str_replace('/', '\\', $name);
        }
        return $name;
    }

    /**
     * 创建目录
     * @access protected
     * @param  string $dirname 目录名称
     * @return void
     */
    protected function checkDirBuild(string $dirname): void
    {
        if (!is_dir($dirname)) {
            mkdir($dirname, 0755, true);
        }
    }

    protected function buildJob(string $toapp, string $preNamespace, string $className): void
    {

        $suffix = '';

        $namespace = trim(implode('\\', array_slice(explode('\\', $className), 0, -1)), '\\');
        $class = str_replace($namespace . '\\', '', $className);

        $filename = $this->basePath . ($toapp ? $toapp . DIRECTORY_SEPARATOR : '') . 'subscribe' . DIRECTORY_SEPARATOR . ltrim(str_replace('\\', '/', $namespace), '/') .($namespace?DIRECTORY_SEPARATOR:''). ucfirst($class) . $suffix . '.php';

        if (!is_file($filename)) {
            $content = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'sysstubs' . DIRECTORY_SEPARATOR . 'subscribe.stub');
    
            $content = preg_replace('/\[\[%[\s\S]*%\]\]/','',$content);
            
            $content = str_replace(
                [
                    '{%namespace%}',
                    '{%className%}',
                ],
                [
                    $preNamespace . '\\' . 'subscribe' . ($namespace ? '\\' . $namespace : ''),
                    ucfirst($class),
                ],
                $content);

            $this->checkDirBuild(dirname($filename));
            file_put_contents($filename, $content);

            $this->output->writeln($filename);
        }else{
            $this->output->writeln('file exist ...'.$filename);
        }
    }

}
